def saludo(saludobebe):
    def funcion_a(funcion_b):
        def funcion_c(*args, **kwargs):
            print('Antes de la ejecución de la función a decorar'+ saludobebe)
            result = funcion_b(*args, **kwargs)
            print('Después de la ejecución de la función a decorar')    

            return result

        return funcion_c
    return funcion_a   

@saludo("Paputa")
def suma(a, b):
    print("ejecución de suma: "+ str(a+b))
    return a + b

print(suma(2,3))
from io import BytesIO
import base64
from os.path import abspath


def pil2base64(img):
    #converts PIL image to datauri
    data = BytesIO()
    img.save(data, "JPEG")
    data64 = base64.b64encode(data.getvalue())
    return u'data:img/jpeg;base64,'+data64.decode('utf-8')

def file2base64(filename):
    with open(abspath(filename), "rb") as img_file:
        my_string = base64.b64encode(img_file.read())
    return u'data:img/jpeg;base64,'+my_string.decode('utf-8')
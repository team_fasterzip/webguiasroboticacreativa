import socket
from flask.helpers import send_file, send_from_directory
from convertpdf_image import GetPDFImage, SavePDFImages
from flask import Flask, jsonify
from base64_conv import file2base64, pil2base64
import glob
from flask_compress import Compress
from os.path import abspath


app = Flask(__name__,
            static_url_path='', 
            static_folder='frontend-app/public')

app.config["COMPRESS_ALGORITHM"] = 'deflate'
#app.config["COMPRESS_LEVEL"] = 9
app.config["COMPRESS_DEFLATE_LEVEL"] = -1

Compress(app)

@app.route('/')
def index():
    return send_file("./frontend-app/public/index.html")

guiasPath = "./Docs/Guias"

@app.route('/GuiasPDFtoJPG', methods=["POST"])
def ConvertirGuias():
    for filename in glob.iglob(guiasPath + "/" + '*.pdf', recursive=False):
        SavePDFImages(filename, f'{guiasPath}/GuiasImg/')
    response = jsonify({"ok": "ok"})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/leerGuia/<guia_id>/<int:page>', methods=["GET"])
def leerGuia(guia_id, page):
    maxpages = 0

    if guia_id == "1":
        maxpages = 9
    elif guia_id == "2":
        maxpages = 15
    elif guia_id == "3":
        maxpages = 14    
    elif guia_id == "D1":
        maxpages = 9
    elif guia_id == "D2":
        maxpages = 1        
    else:
        return jsonify({"error": "guia no encontrada"})

    if page > maxpages:
        return jsonify({"error": "Página no permitida"})

    imagen = file2base64(f"{guiasPath}/GuiasImg/Guia{guia_id}.pdf_page{page}.jpg")

    response = jsonify({"imagen_b64": imagen, "maxPages": maxpages})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/listaChequeo', methods=['GET'])
def listaChequeo():
    ruta = abspath("./Docs") 
    return send_from_directory(ruta, "Listachequeo.pdf")

#Estas lineas de codigo son para crear un servidor local, para produccion usar un servidor WSGI
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

if __name__ == '__main__':
    local_ip = get_ip()
    app.run(host=local_ip, port=8000)
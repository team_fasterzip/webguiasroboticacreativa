
from pdf2image import convert_from_path
import os

def SavePDFImages(pdf_path, destinyDirectory, page = 0): 
    # Store Pdf with convert_from_path function
    images = convert_from_path(pdf_path)
    filename = os.path.basename(pdf_path)
    if page < 1:
        for i in range(len(images)):
        # Save pages as images in the pdf
            images[i].save(destinyDirectory + filename + '_page'+ str(i+1) +'.jpg', 'JPEG', quality=25)
    else:
        images[page].save(destinyDirectory + filename + '_page'+ str(page) +'.jpg', 'JPEG', quality=25)

def GetPDFImage(pdf_path, page = 0): 
    # Store Pdf with convert_from_path function
    images = convert_from_path(pdf_path)
    filename = os.path.basename(pdf_path)
    if page < 1:
        return images
    else:
        return images[page]

